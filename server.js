const express = require("express");
const app = express();
const db = require("./messagesDB");
const cors = require("cors")

app.use(express.json());
app.use(cors());

app.get("/", (req, res) => {
    res.send('Cipher or Decipher password');
});

app.post("/encode", (req, res) => {
    db.getPassword(req.body);
    const encodedMessage = db.cipherMessage(req.body);
    res.send(encodedMessage);
    console.log(encodedMessage);
});

app.post("/decode", (req, res) => {
    const decodedMessage = db.decipherMessage(req.body);
    res.send(decodedMessage);
    console.log(decodedMessage);
});

app.listen(8000, () => {});