const password= [];
const Vigenere =  require("caesar-salad").Vigenere;

module.exports = {
    getPassword(message) {
        password.length = 0;
        password.push(message.password.toString());
    },
    cipherMessage(message) {
        message.message = Vigenere.Cipher(password[0]).crypt(message.message);
        return {encoded: message.message};
    },
    decipherMessage(message) {
        message.message = Vigenere.Decipher(message.password.toString()).crypt(message.message);
        return {decoded: message.message};
    }
};